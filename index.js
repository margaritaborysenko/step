'use strickt'
let tab = function () {
    let tabNav = document.querySelectorAll('.tabs-nav_item');
    let tabContent = document.querySelectorAll('.tab');
    let tabName;

    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav);
    });
    function selectTabNav() {
        tabNav.forEach (item => {
            item.classList.remove('is-active');
        });
        this.classList.add('is-active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent (tabName);
    };
    function selectTabContent (tabName) {
        tabContent.forEach (item => {
            item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
        });
    }
};
tab ();

const PopUp = document.getElementById('my-popup');
const popupToggle = document.getElementById('load-btn');
const popupClose = document.querySelector('.close');

popupToggle.onclick = function () {
    PopUp.style.display = "block";
};
popupClose.onclick = function () {
    PopUp.style.display = "none";
};

let tab1 = function () {
    let tabNav1 = document.querySelectorAll('.gallery-nav_item');
    let tabContent1 = document.querySelectorAll('.tab-content');
    let tabName1;
    tabNav1.forEach(item => {
        item.addEventListener('click', selectTabNav1);
    });
    function selectTabNav1() {
        tabNav1.forEach (item => {
            item.classList.remove('is-active');
        });
        this.classList.add('is-active');
        tabName1 = this.getAttribute('data-tab-name');
        selectTabContent1 (tabName1);
    };
    function selectTabContent1 (tabName1) {
        tabContent1.forEach (item => {
            item.classList.contains(tabName1) ? item.classList.add('is-active') : item.classList.remove('is-active');
        });
    }
};
tab1 ();
